/*
	Cart Class:
	contents: [
		{
			product: prodA,
			quantity: quantity
		}

	];
	totalAmount: number;

	methods:
	addToCart(prodA,quantity)
	showCartContents() - show contents
	clearCartContents() - empties cart contents
	computeTotal() - sums up total price for each cart content
*/
/*
	Product class:
	name: string,
	price: number,
	isActive: Boolean: defaults to true.

	methods:
	archive() - will set isActive to false IF it is active
	updatePrice(newPrice) - replace product price
*/

class Cart {
  constructor() {
    this.contents = [];
    this.totalAmount = 0;
  }

  addToCart(product, quantity) {
    // console.log(product)
    if (product.isActive !== true) {
      console.log(`product not available`);
    } else {
      let container = {};
      container.product = product;
      container.quantity = quantity;

      this.contents.push(container);
      this.computeTotal();
    }

    return this;
  }

  showCartContents() {
    console.log(this.contents);
    return this;
  }

  clearCartContents() {
    this.contents = [];
    this.totalAmount = 0;
    return this;
  }

  computeTotal() {
    let totalAmount = 0;

    this.contents.forEach((cartItem) => {
      // console.log(cartItem); check values
      // console.log(cartItem.product.price); check values
      totalAmount += cartItem.product.price * cartItem.quantity;
    });

    this.totalAmount = totalAmount;
    return this;
  }
}

class Product {
  constructor(name, price) {
    // name: string
    if (typeof name !== "string") {
      this.name = "Product name invalid";
    } else {
      this.name = name;
    }

    // price: number
    if (typeof price !== "number") {
      this.price = "Price invalid";
    } else {
      this.price = price;
    }

    // isActive: default true; but validate first name and price.
    if (typeof name === "string" && typeof price === "number") {
      this.isActive = true;
    } else {
      this.isActive = undefined;
    }
  }

  archive() {
    if (this.isActive === true) {
      this.isActive = false;
    } else {
      console.log(`product already inactive`);
    }

    return this;
  }

  updatePrice(newPrice) {
    if (typeof newPrice !== "number") {
      console.log(`Price invalid`);
    } else {
      this.price = newPrice;
    }

    return this;
  }
}

class Customer {
  constructor(email) {
    if (typeof email !== "string") {
      this.email = "Invalid Email";
    } else {
      this.email = email;
    }
    this.cart = new Cart();
    this.orders = [];
  }

  checkOut() {
    // console.log(this.cart.contents.length);
    // console.log(this.cart.contents.length !== undefined)
    if (
      this.cart.contents.length !== undefined &&
      this.cart.contents.length !== 0
    ) {
      let container = {};

      container.products = this.cart.contents;
      container.totalAmount = this.cart.totalAmount;
      this.orders.push(container);

      // clears cart and transfers it to orders.
      this.cart.clearCartContents();
    } else {
      console.log(`empty cart`);
    }

    return this;
  }
}

const john = new Customer("john@mail.com");
console.log(john);
// console.log(john.checkOut());

// let cart1 = new Cart();
// console.log(cart1);

let prodA = new Product("prodA", 100);
let prodB = new Product("prodB", 50);
console.log(prodA);
console.log(prodB);

// cart1.addToCart(prodA,3);
// cart1.addToCart(prodB,2);

// cart1.showCartContents();

// cart1.computeTotal();

// cart1.clearCartContents();

// prodA.updatePrice(99);

// prodA.archive();
